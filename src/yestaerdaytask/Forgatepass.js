import React from 'react';
import {Link} from 'react-router-dom';

const Forgatepass = (props) => {
  return (
    <>
      <div class="section mt-2">
            <div class="card" style={{width:"30rem"}}>
                <div class="card-body">
                    <h5 class="card-title">Forgate Password</h5>
                    <form >
                      <div className="form-row">
                        <div className="form-group col-12">
                        
                          <input type="email" name="email" placeholder="Enter Vaild Email" className="form-control"/>

                        </div>

                      </div>
                      <div className="form-row">
                        <div className="form-group col-12">
                        <button type="button" class="btn btn-text-primary shadowed me-1 mb-1 mt-1">Submit</button>
                        </div>

                      </div>
                    <div className="form-group">
                      Alraedy have account? <Link to="/Login">Sign in</Link>

                    </div>

                    </form>
                    
                </div>
            </div>
        </div>
      
    </>
  )
}

export default Forgatepass
