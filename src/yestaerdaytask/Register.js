import React from 'react';
//import { Link } from 'react-router-dom';

const Register = (props) => {
  return (
    <>
    <div className="login-form">
              <div className="section">
                <h1>{props.h1}</h1>
             </div>
          <div className="section mt-2 mb-5">
              <form>
                    <div className="form-group boxed">
                        <div className="mt-2">
                        {props.children}
                           
                        </div>
                    </div>
                   
                </form>
            </div>
        </div>

    
</>
  )
}

export default Register
