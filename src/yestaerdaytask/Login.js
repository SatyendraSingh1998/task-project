import React from 'react';
import {Link} from 'react-router-dom';


const Singup = () => {
  return (
    <>
    <div id="appCapsule" className="pt-15">
<div className="login-form mt-1">
    <div className="section mt-1">
        <h4>Fill the form to Sign in</h4>
    </div>
    <div className="section mt-1 mb-5">
        <form action="app-pages.html">
            <div className="form-group boxed">
                <div className="input-wrapper">
                    <input type="email" className="form-control" id="email1" placeholder="Email address"/>
                    <i className="clear-input">
                        <ion-icon name="close-circle"></ion-icon>
                    </i>
                </div>
            </div>

            <div className="form-group boxed">
                <div className="input-wrapper">
                    <input type="password" className="form-control" id="password1" placeholder="Password" autocomplete="off" />
                    <i className="clear-input">
                        <ion-icon name="close-circle"></ion-icon>
                    </i>
                </div>
               
            </div> 
            
            <div className="form-links mt-2">
                <div>
                <button type="button" class="btn btn-text-primary shadowed me-1 mb-1 mt-1">
                    <Link to="/Dasboard">Sign in</Link>
                </button>
               
                </div>
                <div>
                    <Link to="/">Back....?</Link>
                    <Link to="/Forgatepass" className="text-muted">Forgot Password?</Link>
                    </div>
            </div>
            
           

        </form>
    </div>
</div>


</div>
      
    </>
  )
}

export default Singup
