import React  from 'react'

const InputFiels = (props) => {
  return (
   <>
   
       <input type={props.type} placeholder={props.placeholder}
       name={props.name}
       onChange={props.onChange} className={props.class} autoComplete={props.auto}
      /> 
      
    </>
  )
}

export default InputFiels
